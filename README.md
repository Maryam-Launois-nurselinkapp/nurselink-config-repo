# Nurselink-config-repo

In this project, you will find external properties files used by the following REST APIs :

* [nurselink-ms-job](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-job)
* [nurselink-ms-schedule](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-schedule) 
* [nurselink-ms-user](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-user)
* [nurselink-web-client](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-web-client)


